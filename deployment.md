## 项目部署指南

部署流程：
 * 1. 申请微信小程序（获得appid）
```
申请地址：https://mp.weixin.qq.com/wxopen/waregister?action=step1&token=&lang=zh_CN
```
 * 2. 下载微信开发着工具
```
下载地址： https://developers.weixin.qq.com/miniprogram/dev/devtools/stable.html
```
 * 3. clone 代码
```
克隆地址：https://git.weixin.qq.com/wx_wx8d38f9257b9863b3/mp-easynotes.git
```

 * 5. 配置config文件
 ```
  打开 /miniprogram/common/config.js
  输入 和风天气的apiKey
  输入 腾讯地图的apikey
  输入 云环境的id
 ```

 * 6. 配置云环境
```
新建表 ["notes","user","record"]
新建云储存 ["images","record","static","videos"]
上传云函数 cloudfunctions 目录下的所有云函数
```

 * 7. 在小程序登录后添加插件
```
小程序主页=> 设置=> 第三方设置=> 插件管理=添加插件=>[
  OCR支持,
  微信同声传译
]
```
 * 8. 添加合法域名
```
小程序登录主页=> 开发菜单=> 开发设置=>服务器域名=>添加=>[	
https://apis.map.qq.com //腾讯地图
https://devapi.heweather.net //和风天气
https://geoapi.heweather.net //和风天气
]
```
 * 9. 在miniprogram目录下打开终端 输入：npm i
 
 * 10. 打开微信开发者工具，然后选择工具中的构建npm

* 11. 微信开放社区购买内容弄
```

https://developers.weixin.qq.com/community/servicemarket/detail/0008222a430fb072202a8e0625bc15 //语音识别一句话识别服务

```
 * 12. 点击编译按钮，开始体验小记易