# 小记易更新日志
|  记录   | 作者  |  时间 | 内容  |
|  ----  | ----  | ----  | ----  |
|bc1bcb0|junfenginfinit|27 minutes ago|优化逻辑|
|5fd13f3|junfenginfinit|46 minutes ago|设置详情分享功能|
|c669c97|junfenginfinit|10 hours ago|代码格式化|
|fa33ab4|junfenginfinit|10 hours ago|解决冲突|
|d725ea3|junfenginfinit|11 hours ago|新增内容安全检测|
|d3c9866|慌雞|12 hours ago|修复资源统计错误问题|
|f179ca1|慌雞|13 hours ago|合并2020年09月19日22:26:27|
|d3e4e84|慌雞|13 hours ago|首页样式微调|
|c8e3f19|慌雞|13 hours ago|合并 首页样式微调|
|e0305f8|慌雞|15 hours ago|更新编译模式|
|93f8d16|慌雞|15 hours ago|资源长度显示|
|475333c|慌雞|15 hours ago|content.videos 初始化优化|
|bbcfd8d|慌雞|15 hours ago|页面加载动画时长调整|
|d39f8ae|慌雞|15 hours ago|语音播放暂停bug修复|
|7713e86|慌雞|15 hours ago|语音识别修复|
|908f1e8|junfenginfinit|16 hours ago|首页数据定位|
|5f2bc73|慌雞|17 hours ago|录音存储路径修复|
|39c5427|慌雞|17 hours ago|录音删除修复|
|4eb4791|慌雞|17 hours ago|隐藏删除按钮选项|
|d163034|慌雞|17 hours ago|新增  资源为空时列表初始化|
|fde611e|junfenginfinit|17 hours ago|优化详情页状态栏背景|
|6310274|junfenginfinit|17 hours ago|切换appid|
|8c6428d|junfenginfinit|20 hours ago|优化用户体验|
|22f3fda|慌雞|22 hours ago|合并冲突2020年09月19日13:07|
|d6978e6|lianfangti|23 hours ago|新增编辑权限判断|
|4ba672e|lianfangti|23 hours ago|bigbang组件中 词过多时显示异常问题|
|5116924|lianfangti|23 hours ago|开发者工具中不震动|
|7bb6402|lianfangti|23 hours ago|更新个人页面说明|
|ac79682|junfenginfinit|23 hours ago|优化项目|
|d4e6938|lianfangti|25 hours ago|Merge refs/remotes/origin/master into refs/heads/lft|
|0f1dd9f|lianfangti|25 hours ago|vbs方法增加判断 在开发者工具中不震动|
|84ad33c|junfenginfinit|34 hours ago|解决冲突，详情增加角标|
|3514f49|lianfangti|2 days ago|npm 构建|
|8475a91|lft|2 days ago|更改 navList在config中的名称为NOTES_CATE|
|cd7387e|lft|2 days ago|调整编译模式|
|14aee8f|lft|2 days ago|新增根据不同文章类型顶部栏初始化|
|95769a2|lft|2 days ago|更新详情页顶部栏根据类型显示|
|fd0b1c1|lft|2 days ago|洗澡能crypto-js 加密工具|
|ab09532|lft|2 days ago|优化toast弹窗方法|
|284b508|lft|2 days ago|修改文档类型配置格式|
|f75b30c|lft|2 days ago|新增api文件|
|a9ccba6|lft|2 days ago|云函数更新bug修复|
|f9d3b60|lianfangti|2 days ago|圆角拓展|
|10d5ee4|lianfangti|2 days ago|修改预览模式|
|4d4b995|lianfangti|2 days ago|新增圆角拓展|
|0b2f9fc|lianfangti|2 days ago|详情页制作|
|64fd508|lianfangti|3 days ago|Merge refs/heads/master into refs/heads/lft|
|d11adff|小生呦|3 days ago|优化代码|
|7a79b2e|lft|3 days ago|新增插入视频功能|
|dd71ce0|lft|3 days ago|调整语音播放条样式|
|5d31233|lft|3 days ago|新增  RESOURSES   资源类型配置|
|8e84067|lianfangti|3 days ago|新增资源图标 新增位置插入 更改录音布局|
|8c10191|lianfangti|3 days ago|底部新增  资源图标|
|4e4e4fe|小生呦|3 days ago|详情显示|
|0173d45|小生呦|3 days ago|触底加载，下拉刷新，首页数据分类|
|c75334b|小生呦|4 days ago|恢复方法|
|16a7586|lianfangti|4 days ago|合并2020年09月16日20:36:48|
|55a1f00|lianfangti|4 days ago|处理意外type|
|5fae9b2|小生呦|4 days ago|fix bug|
|d7664fc|lianfangti|4 days ago|Merge branch 'master' of https://git.weixin.qq.com/leedaisen/easy-nodes|
|7ffbc0a|lianfangti|4 days ago|合并2020年09月16日20:26:14|
|76061d4|lianfangti|4 days ago|冲突解决|
|b58c1da|lianfangti|4 days ago|Merge refs/heads/master into refs/heads/lft|
|2acd390|lianfangti|4 days ago|合并|
|aded468|lianfangti|4 days ago|新增key|
|9662830|lianfangti|4 days ago|放出openid|
|155ecd9|小生呦|4 days ago|完善登录|
|18394b9|lft|4 days ago|保存功能调试顶部栏 天气位置信息显示|
|5daddc6|lft|4 days ago|预览模式更新|
|5ccdb52|lft|4 days ago|新增 getWeather|
|66f6003|lft|4 days ago|新增HE_WEATHER_KEY 配置|
|0419060|junfenginfinit|4 days ago|首页获取数据|
|c44cef2|junfenginfinit|4 days ago|fix bug|
|15da4c6|lianfangti|4 days ago|代码合并 冲突解决 2020年09月16日00:23:53|
|95a8469|lianfangti|4 days ago|语音识别结果对接Bigbang|
|ce3b702|lianfangti|4 days ago|去除日志输出|
|f0ba183|lianfangti|4 days ago|去除打印日志|
|971f17f|lianfangti|4 days ago|新增getLocationInfo方法|
|0f8571f|lianfangti|4 days ago|新增TENCENT_MAP_KEY 配置|
|c775b6c|小生呦|4 days ago|删除无用云函数|
|a00f986|小生呦|5 days ago|完成登录方法|
|ffb0516|unknown|5 days ago|editor页面优化|
|04b345b|unknown|5 days ago|更新日期格式化 DateFormat|
|1af1305|lft|5 days ago|暂存|
|f7a2f7d|unknown|5 days ago|暂存|
|931bdbb|小生呦|6 days ago|优化代码，优化用户体验|
|2323507|小生呦|6 days ago|完成扫描的方法|
|8edd3f7|lft|6 days ago|图片OCR合并|
|505bdfb|unknown|6 days ago|临时保存|
|f097d16|小生呦|6 days ago|删除图片|
|3926f4d|小生呦|6 days ago|2020年9月14日 01:26:37|
|d90a465|小生呦|7 days ago|优化首页|
|c8ca506|小生呦|8 days ago|首页添加滚动效果|
|6e7177d|练方梯|9 days ago|Merge remote-tracking branch 'refs/remotes/origin/master'|
|101508d|unknown|9 days ago|合并冲突解决|
|a908c37|小生呦|9 days ago|解决冲突|
|e9e5f9b|小生呦|9 days ago|解决冲突|
|74d034b|小生呦|10 days ago|解决冲突|
|e0e0abb|lianfangti|10 days ago|解决冲突|
|3788afd|lianfangti|10 days ago|删除冲突文件|
|3f5e265|小生呦|10 days ago|优化界面|
|dd4b775|小生呦|10 days ago|解决冲突|
|4cc8857|lft|10 days ago|暂存|
|d550f51|lianfangti|10 days ago|冲突解决|
|709be32|lianfangti|10 days ago|代码合并|
|8ec5028|lianfangti|10 days ago|合并|
|fa30319|lianfangti|10 days ago|合并|
|23ba65b|lianfangti|10 days ago|文件删除|
|431e245|lianfangti|10 days ago|OCR识别调试中|
|ec29af6|lianfangti|10 days ago|小生改动|
|a6064b4|lianfangti|10 days ago|新增popup组件|
|32d09cc|lianfangti|10 days ago|README更新|
|c68d194|lianfangti|10 days ago|新增分词组件|
|46df6dd|lianfangti|10 days ago|配置更新|
|fe1eb28|lianfangti|10 days ago|新增公共方法|
|86089c1|lianfangti|10 days ago|新增分词云函数|
|5a7e126|小生呦|11 days ago|优化组件|
|b4b271f|小生呦|11 days ago|优化代码体积|
|c6acd94|小生呦|11 days ago|优化搜索动画，个人中心完成一部分|
|298ee04|小生呦|11 days ago|npm 更新了|
|6c32333|lianfangti|11 days ago|.|
|3aafde9|小生呦~|11 days ago|优化用户体验|
|ba3b0d9|小生呦~|11 days ago|完成主页样式|
|1b174a2|小生呦~|12 days ago|优化代码|
|d7d5585|lianfangti|12 days ago|Merge refs/remotes/origin/master into refs/heads/lft|
|fcaa79c|lianfangti|12 days ago|.text-sub 文字下标|
|0bcce19|lianfangti|12 days ago|引入动画 animaltion|
|8ea4c58|lianfangti|12 days ago|合并图标|
|332c9f6|lianfangti|12 days ago|图像扫描功能 颜色选择器|
|89078f8|lianfangti|12 days ago|没有改变|
|c8b9ccc|lianfangti|12 days ago|Voice音频播放组件封装|
|40aafa1|lianfangti|12 days ago|ColorPicker 颜色选择器新增|
|1d8454a|lianfangti|12 days ago|utils.js新增公共方法|
|d9a372d|unknown|2 weeks ago|按钮跳转方法|
|1e87544|unknown|3 weeks ago|弹窗组件|
|dd6d5ea|unknown|3 weeks ago|Merge refs/remotes/origin/lft into refs/heads/master|
|36b076d|unknown|3 weeks ago|增加弹窗|
|dd4f7ad|lianfangti|3 weeks ago|Merge refs/remotes/origin/master into refs/heads/lft|
|7426316|lianfangti|3 weeks ago|编辑器开发中|
|40014a7|unknown|3 weeks ago|完成首页头部|
|b9b3e0e|lianfangti|3 weeks ago|引入新图标|
|91f0220|lianfangti|3 weeks ago|编辑页新增|
|026e779|lianfangti|3 weeks ago|录音调试|
|9a733f4|lianfangti|3 weeks ago|没有改动|
|ad3b57f|lianfangti|3 weeks ago|引入miniprogram-api-promise包|
|89cb4dd|lianfangti|3 weeks ago|没有改动|
|7ac7f2d|lianfangti|3 weeks ago|npm 拓展包构建|
|dbc8edf|lianfangti|3 weeks ago|Merge refs/remotes/origin/master into refs/heads/master|
|38608f7|lianfangti|3 weeks ago|ColorUI代码更新|
|454f47a|lianfangti|3 weeks ago|新增录音上传存储至数据库云函数|
|8000c32|lianfangti|3 weeks ago|新增录音列表获取云函数|
|400ebba|lianfangti|3 weeks ago|引用语音识别和OCR识别插件|
|6892994|lianfangti|3 weeks ago|新增demo页面|
|d75cf0b|unknown|4 weeks ago|项目初构|
|693b5a0|lianfangti|4 weeks ago|Initial Commit|