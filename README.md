# 小记易

## 2020腾讯云开发线上比赛参赛作品

## 产品起源：
- 无意中看到的无用说明书，
- 回想起在课堂记不住重点的样子
- 在会议中，需要形成会议记录
- 想要记住生活中的美好

## 项目特点：
- 文字大爆炸功能（一句话爆炸成单词。选到你需要的文字。）
- ocr 功能 （图片识别转成文字。让输入更加简单。）
- 语音识别功能（懒得打字就说出来。说出来我帮你写。）
- 插入位置功能（多年以后，看到日记，不如导航到这个地方看看。）
- 插入天气功能（感谢和风天气，提供免费的天气数据）
- 插入视频功能（更好的用户体验）

## 依赖
- 腾讯云开发，云函数，云储存

## 定位：
- 解决会议记录使用传统记录软件，不够快，不够舒服的问题
- 帮助学生更好的去理解学校的内容。
- 方便用户更好的记录每日的生活
- 企业的电子说明书，减少资源浪费，提升用户体验

## 使用框架
- miniprogram-computed
- miniprogram-watch

## 使用插件
- 微信同声传译插件
- OCR支持

## 注意事项
- 需要使用微信小程序的npm

## 部署说明
这个项目具体的部署参考[deployment.md](/deployment.md)

## 代码贡献
如果您想贡献代码，可以阅读[contributing.md](/contributing.md)

## LICENSE
了解开源协议，可以查看[Apache-2.0](LICENSE)

## 更新日志
希望查看更新日志：[changelog.md](changelog.md)

## Bug 反馈

如果有 Bug ，可直接去[issues page](/issues/new)提issue。

## 联系方式

有任何问题，可以通过下方的联系方式联系我们。

  · Wechat:Yay_ls
  
  · Wechat:LIANFANGTI97 


## 项目截图

<img src="https://s1.ax1x.com/2020/09/16/wgBg9x.png" width="49%" alt="项目截图 1.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgB641.png" width="49%" alt="项目截图 2.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgBRgK.png" width="49%" alt="项目截图 3.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgBWjO.png" width="49%" alt="项目截图 4.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgByNR.png" width="49%" alt="项目截图 5.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgBhuD.png" width="49%" alt="项目截图 6.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgB4De.png" width="49%" alt="项目截图 7.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgB5HH.png" width="49%" alt="项目截图 8.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgBoEd.png" width="49%" alt="项目截图 9.0 ">
<img src="https://s1.ax1x.com/2020/09/16/wgDfGq.png" width="49%" alt="项目截图 10.0 ">

## 项目相关

//

·[开源地址 ：微信代码仓库(需要是小程序的开发者才可以查看)](https://git.weixin.qq.com/leedaisen/easy-nodes)

·[开源地址 Gitee](https://gitee.com/leedaisen/easy-notes)

·[演示视频 ：腾讯视频](http://m.v.qq.com/play/play.html?vid=p3153ssyje2&url_from=share&second_share=0&share_from=copy)

###  产品架构图（ 1.0.1）

<img src="https://s1.ax1x.com/2020/09/16/wgwGrQ.png" width="100%" alt="产品架构图 1.0 ">
