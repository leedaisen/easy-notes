/*
* 删除一条或者多条记录
  传入参数e是对象,结构如下
  e._table是数据表名称
  e._ids是记录[id1,id2....]
*/
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
exports.main = async (e, context) => {
  let table = e._table || ''
  delete (e._table)
  if (!table) {
    console.error('Table name :' + table + ' missing')
    return false;
  }
  let ids = e._ids || []
  for(let i=0;i<ids.length;i++){
    try {
      await db.collection(table).doc(ids[i]).remove()
    } catch (e) {
      console.error(e)
    }
  }
  return true
}