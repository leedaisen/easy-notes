// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event) => {
  try {
    let result = '';
    if (event.content) {
      result = await cloud.openapi.security.msgSecCheck({
        content: event.content
      });
    } else if (event.image) {
      result = await cloud.openapi.security.mediaCheckAsync({
        media: {
          media_type: 2,
          media_url: event.image
        }
      })
    } else if (event.record) {
      result = await cloud.openapi.security.mediaCheckAsync({
        media: {
          media_type: 1,
          media_url: event.record
        }
      })
    }
    return {
      result
    }
  } catch (error) {
    return {
      error
    }
  }
}