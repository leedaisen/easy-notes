/*
* 新建或者更新数据表
  传入参数e是包含key-value数据的对象
  e._table是数据表名称
  e._id是记录id
  返回值：更新或者新增的记录
  update：20190221
*/
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
let timenow = (new Date()).valueOf()
exports.main = async (e, context) => {
  delete(e.userInfo)
  let table = e._table || ''
  delete(e._table)
  if (!table) {
    console.error('Table name :' + table + ' missing')
    return false;
  }
  let id = e._id || ''
  delete(e._id)
  let row; //返回值
  if (id) { //检查记录存在
    row = await db.collection(table).doc(id).get()
    if (!row.data._id) {
      console.error('Record in ' + table + ' not found')
      return false;
    }
    //更新
    try {
      e.modifytime = timenow
      const res = await db.collection(table).doc(id).update({
        data: e
      })
      row = await db.collection(table).doc(id).get()
      return row.data || {};
    } catch (err) {
      console.error(err)
    }
  } else { //新建
    try {
      e.createtime = timenow
      e.modifytime = timenow
      const res = await db.collection(table).add({
        data: e
      })
      row = await db.collection(table).doc(res._id).get()
      return row.data || {};
    } catch (err) {
      console.error(err)
    }
  }
}