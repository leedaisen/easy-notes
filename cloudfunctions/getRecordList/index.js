// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db = cloud.database();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  let result = await db.collection("record").get();
  result = result.data;
  if (!result.length) {
    return result
  } //如果为空直接返回
  let getTempURLResult = await cloud.getTempFileURL({
    fileList: result.map(item => item.fileID)
  })
  let srcs = getTempURLResult.fileList.map(item => item.tempFileURL)
  result = result.map((item, index) => {
    item.src = srcs[index];
    return item;
  })
  return result
}