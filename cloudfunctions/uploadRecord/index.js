// 云函数入口文件
const cloud = require('wx-server-sdk')
// const load = require('audio-loader');
cloud.init()
const db = cloud.database();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  let {
    fileID,
    duration
  } = event;
  let insertResult = await db.collection("record").add({
    data: {
      fileID,
      duration
    }
  })
  return {
    ...insertResult,
  }
}