/**删除文件
 * 参数e是一个数组，包含文件id
 */
const cloud = require('wx-server-sdk')
cloud.init()
exports.main = async (e, context) => {
  const fileIDs =typeof(e.ids)==='string'?[e.ids]:e.ids
  const result = await cloud.deleteFile({
    fileList: fileIDs,
  })
  return result.fileList
}