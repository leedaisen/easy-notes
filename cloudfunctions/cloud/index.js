/* 代请求的方法 */
const request = require('request');
exports.main = (evt, ctx) => {
  return new Promise((ress, errr) => {
    request(evt.options, (err, res, body) => {
      if (err) return errr(err);
      ress(res);
    })
  });
}
