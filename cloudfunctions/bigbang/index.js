// 云函数入口文件
const cloud = require('wx-server-sdk')
const Segment = require('segment');
const bigbang =  new Segment();
bigbang.useDefault();

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  // bigbang.load();
  let {text} = event;
  return bigbang.doSegment(text);
}