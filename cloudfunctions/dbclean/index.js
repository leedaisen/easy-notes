/*
* 删除过期数据
*/
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const _ = db.command
const timenow = (new Date()).valueOf()
exports.main = async (e, context) => {
  //删除过期play
  try {
      await db.collection('play').where({
        confirmShare:0,
        modifytime:_.lt(timenow-86400000)
      }).remove()
  } catch (e) {
      console.error(e)
  }
  try {
    await db.collection('formid').where({
      modifytime: _.lt(timenow - 7*86405000)
    }).remove()
  } catch (e) {
    console.error(e)
  }
  return true
}