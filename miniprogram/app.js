//app.js
import {
  CLOUND_ENV
} from '/common/config'
App({
  onLaunch: function () {
    // 登录 
    // 云开发
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: CLOUND_ENV,
        traceUser: true,
      })
    }
  },
  update: function () {
    const updateManager = wx.getUpdateManager()
    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function (res) {
          if (res.confirm) {
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(function () {
      wx.showLoading({
        title: '新版本下载失败，请重启!',
      })
    })
  },
  onError: function (msg) {
    wx.showModal({
      title: '小程序发生未知错误',
      content: msg,
      confirmText: '重启一下',
      confirmColor: '#000',
      success: function (res) {
        if (res.confirm) {
          wx.reLaunch({
            url: '/pages/index/index'
          })
        }
      }
    })
  },
  globalData: {}
})