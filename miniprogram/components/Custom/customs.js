const app = getApp();
Component({
  options: {
    addGlobalClass: true,
    multipleSlots: true
  },
  properties: {
    bgColor: {
      type: String,
      default: ''
    },
    isCustom: {
      type: [Boolean, String],
      default: false
    },
    isShare: {
      type: [Boolean, String],
      default: false
    },
    isNull: {
      type: [Boolean, String],
      default: false
    },
    isBack: {
      type: [Boolean, String],
      default: false
    },
    bgImage: {
      type: String,
      default: ''
    },
  },
  data: {
    StatusBar: 0,
    CustomBar: 0,
    Custom: 0
  },

  methods: {
    BackPage() {
      wx.navigateBack({ delta: 1 });
    },
    toHome() {
      wx.reLaunch({
        url: '/pages/index/index',
      })
    }
  },
  /*组件生命周期*/
  lifetimes: {
    attached() {
      let that = this
      wx.getSystemInfo({
        success: e => {
         let StatusBar = e.statusBarHeight;
         let capsule = wx.getMenuButtonBoundingClientRect();
          if (capsule) {
            that.setData({
              StatusBar:StatusBar,
              Custom:capsule,
              CustomBar:capsule.bottom + capsule.top - e.statusBarHeight
            })
          } else {
            that.setData({
              CustomBar:e.statusBarHeight + 50
            })
          }
        }
      })
    },
    error(e) {},
  }
})