const { getAudioDuration, speechToText } = require("../../common/utils");
const computedBehavior = require('miniprogram-computed');//computed 拓展

// components/Voice/index.js
Component({
  behaviors: [computedBehavior],
  options:{
    addGlobalClass: true,
  },
  /**
   * 组件的属性列表
   */
  properties: {
    fileID:{
      type:String,
      observer(nv,ov){
        // console.log("数据值发生变化啊",nv,this)
        this.init(nv);
      }
    },
    duration:{
      type:String,
    },
    class:{
      type:String, 
    },
    remove:{
        type:Boolean,
        value:true
    }

  },
  computed:{
    curtime(data){
       let time  = data.duration - data.currentTime
      //  console.log("播放时间计算",time)
       return Math.round(time);
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    curSpeed:2,//当前播放速度索引
    speed:["0.5","0.75","1.0","1.25","1.5","2"],//播放速度选择
    showTextContent:false,//语音转文字
    showAnimation:'',
    playing:false,//是否播放中
    duration:0,//播放时长,
    currentTime:0,
    speechToTextContent:'',//语音识别内容
    url:'',//音频文件路径
    traning:false,//转换中

  },
  created(){
    setTimeout(()=>{
      // console.log("当前传入的iclass",this.data.class)
    },500);

  
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //播放
    handlePlay(){
        if(this.data.playing){
          this.innerAudioContext.pause();
        }else{
          try{
            this.innerAudioContext.play(); 
          }catch(e){
             console.error("错误:","音频播放失败");
          }
        }
    },
    //删除音频
    handleRemove(e){
      this.triggerEvent("remove")
    },
    //音频初始化
   async  init(fileID){
       //获取临时播放路径
       let {fileList}= await wx.cloud.getTempFileURL({
         fileList:[fileID]
       })
       let [{tempFileURL}] = fileList
       this.setData({
         url:tempFileURL
       })
       let innerAudioContext = wx.createInnerAudioContext();
       innerAudioContext.src =  tempFileURL;
       //开始播放
       innerAudioContext.onPlay((res)=>{
         this.setData({playing:true});
       });
       //播放中
       innerAudioContext.onTimeUpdate(res=>{
         let {currentTime} = innerAudioContext;
         this.setData({currentTime})
       });
       //暂停播放
       innerAudioContext.onPause(_=>{
        this.setData({ playing:false})
       })
       //播放结束
       innerAudioContext.onEnded(res=>{
         this.setData({playing:false,currentTime:0});
       });



      this.innerAudioContext = innerAudioContext;
      let duration =this.data.duration
      duration = Math.round(duration)
      
      this.setData({duration})

      //  console.log("音频初始化:",`文件播放路径:${tempFileURL}\n`,`文件ID:${fileID}\n`,`录音时长:${duration}`);
       
    },
    // 播放速度变化
    handleSpeedPickerChange({detail}){
      let {value:curSpeed } =detail;
      this.setData({curSpeed})  
    },
    // 切换语音识别内容显示隐藏
    async handleSwitchShowText(){
      this.setData({traning:true});
      let showTextContent = this.data.showTextContent;
      let {url,fileID}  =this.data;
      
      try{
        let speechToTextContent= await speechToText({url,fileID});
        this.triggerEvent("onASR",speechToTextContent)
        this.setData({showTextContent:!showTextContent,speechToTextContent,traning:false});
      }catch(e){
        console.error("语音识别失败",e);
        this.setData({traning:false})
        
      }
    },
    //隐藏识别文字
    handleHideText(){
      this.setData({showTextContent:false})
    },
    //拷贝识别文字到剪切板
    handleCopyText(){
      wx.setClipboardData({
        data: this.data.speechToTextContent,
        success(){
          wx.showToast({ title: '拷贝成功' })
        }
      })
    
    }

  }
})
