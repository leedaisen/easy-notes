// components/popup/index.js
/**
 * 组件说明
 * Popup弹出层
 * 属性
 * title  
 * 弹窗标题
 * show
 * 控制弹窗显示/隐藏
 * className
 * 自定义样式
 * position
 * 弹出位置   bottom top right left  center
 * mask 
 * 
 * disableScroll
 * 事件
 * onClose
 * 弹窗关闭事件
 * 
 * 
 */


Component({
  /**
   * 组件的属性列表
   */
  options:{
    addGlobalClass: true,
  },
  properties: {
    className:{
      type:String,
      value:''
    },
    title:{
      type:String,
      value:'标题'
    },
    show:Boolean,
    position:{
      type:String,
      value:'bottom'
    },
    mask:{
      type:Boolean,
      value:true
    },
    animation:{
      type:Boolean,
      value:true
    },
    disableScroll:{
      type:Boolean,
      value:true
    },
    


  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onMaskTap() {
        this.handleClose();
        this.triggerEvent("onClose");
    },
    handleClose(){
      this.setData({
        show:false
      })
    }

  }
})
