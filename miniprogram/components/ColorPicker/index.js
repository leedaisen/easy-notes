// components/ColorPicker/index.js
Component({
  options:{
    addGlobalClass: true,
  },
  /**
   * 组件的属性列表
   */
  properties: {
    colors:{
      type:Array,
      value:[ //默认颜色列表
        {name:'black',value:'#333333'},
        {name:'red',value:'#e54d42'},
        {name:'orange',value:'#f37b1d'},
        {name:'yellow',value:'#fbbd08'},
        {name:'green',value:'#39b54a'},
        {name:'blue',value:'#0081ff'},
        {name:'purple',value:'#6739b6'},
      ]
    },
    custom:{//是否显示自定义颜色按钮
      type:Array,
      value:false
    }

  },

  /**
   * 组件的初始数据
   */
  data: {
    cur:0,
    isBack:false,//是否为背景色
    fore:'black',
    back:''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //清除背景色
    handleCleanBackground(){
      wx.vibrateShort();
      this.setData({
        back:''
      })
      
    },
    // 前景色背景色转换
    handleSwitch(){
      
      this.setData({
        isBack:!this.data.isBack
      })
      wx.showToast({
        title: this.data.isBack ?'背景色':'前景色',
        icon:'none'
      });

    },
    //颜色切换
    handleColorChange({currentTarget:{dataset}}){
      let {index,item} = dataset;
      let {isBack} = this.data;
      wx.vibrateShort()
      this.setData({
        cur:index,
        [`${isBack?'back':'fore'}`]:item.name
      });
      let {fore,back} = this.data;
      if(fore === back){
        fore = 'gray';
        this.setData({fore})
      }
      let color = {
        ...item,
        fore,
        back,
        isBack
      }
      this.triggerEvent("Change",color)
      console.log("Change",color)
    }

  }
})
