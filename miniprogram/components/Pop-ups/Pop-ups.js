/** 
 * 小程序自定义弹出层组件

 * 配置参数：
 * PopUpType：弹框位置
 * 可选值 ： top上弹框 bottom下弹框 left左弹框 right 右弹框 content 中心弹框 
 * 
 * PopUpBg:定义背景颜色 
 * 可选值：字符串 如：#fff;默认白色
 *
 * PopUpList:定义所有使用的弹窗名
 * 可选值：数组 如：['pay','car']
 *
 * PopUpName:定义弹出层的名称
 * 可选值：字符串 如：pay
 *
 * PopUpTitle ：弹出层标题 默认为空
 * 可选值：字符串 如：#分类选择;
 * 
 * PopUpRadius: 是否带圆角
 * 可选值：布尔型  ture or false 默认不带
 * 
 * PopUpHeight:弹出框高度
 * 可选值:字符串 如：800rpx;
 * 
 * 子节点：content
 * 
 * 食用方法：
 * 如有components将PopupView放入components
 * 
 * 页面中Json或者app.json中引入组件
 * usingComponents：{
 *  "PopUPView":"/components/PopupView/PopupView"
 * }
 * 
 * 
 * 页面Json代码：
 * 
 *  <PopUPView PopupType="bottom" >
 *    <view slot="content">
 *       这里加入你要在弹窗里显示的内容
 *   </view>
 *  </PopUPView>
 * 
 * 页面JS代码
 *   popUp(e) {
 *   this.setData({
 *    PopUpName: e.currentTarget.id,
 *    PopUpType: e.currentTarget.dataset.type
 *   })
 * }
 * 
 * 页面Wxml代码
 * 
 * 
 * <PopUPView PopUpType="{{PopUpType}}" PopUpName="{{PopUpName}}" PopUpList="{{PopUpList}}">
 *  <view slot="content">
 *    <view class="padding">
 *     这里加入你要在弹窗里显示的内容
 *    </view>
 *  </view>
 * </PopUPView>
 * 
 */
Component({
  options: {
    multipleSlots: true
  },
  properties: {
    PopUpTitle: {
      type: String,
      default: ''
    },
    
    PopUpName: {
      type: String,
      default: null
      
    },

    PopUpList: {
      type: Array,
      default: []
    },

    PopUpBg: {
      type: String,
      default: '#fff;'
    },

    PopUpType: {
      type: String,
      default: 'bottom',
      observer: function(newVal, oldVal) {
        // console.log(newVal, oldVal)
        this.setData({
          PopupType: newVal
        })
      }
    },
  },

  data: {
  },

  methods: {
    showModal(e) {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    },
    con(e) {
      // this.setData({
      //   PopUpType:this.data.PopUpType
      // })
    },
    hideModal(e) {
      this.setData({
        PopUpName: null
      })
    },
  }
})