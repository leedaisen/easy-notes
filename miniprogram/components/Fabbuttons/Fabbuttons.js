import { vbs } from "../../common/utils"
Component({
  properties: {
    menuList: { // 属性名
      type: Array,
      value: []
    },
  },
  data: {
    isActive: false
  },
  methods: {
    tabFabBtn() {
      vbs()
      this.setData({
        isActive: !this.data.isActive
      })
    },
    setBtn(e) {
      vbs()
      let data = this.data.menuList
      var myEventDetail = [data, e.currentTarget.id]
      var myEventOption = {}
      this.triggerEvent('_setBtn', myEventDetail, myEventOption)
    }
  }
})