import {
  dbone
} from "../requset";
import {
  makeSign,
  wxp
} from "../utils";

/**
 * 获取文章详情
 * @param {String} id 
 */
export async function getNotesDetail(id) {
  let result = await dbone("notes", id);
  // let {
  //   resourse
  // } = result;
  result.id = result._id
  return result;
}

/**
 * 保存文章（新建 编辑二合一  根据是否传入id判断）
 * @param {Object} content  
 */

export async function saveNotes(content) {
  return await wx.cloud.callFunction({
    name: 'dbsave',
    data: {
      _table: 'notes',
      ...content
    }
  });
}

export async function msgCheck(content) {
  // 检测标题是否违规
  let title = await wx.cloud.callFunction({
    name: "msgCheck",
    data: {
      content: content.title,
    }
  })
  // 检测内容是否违规
  let html = await wx.cloud.callFunction({
    name: "msgCheck",
    data: {
      content: content.html,
    }
  })
  // 检测录音是否违规
  // if (content.resourse.records && content.resourse.records.length) {
  //   for (var i = 0; i < content.resourse.records.length; i++) {
  //     var records = []
  //     records[i] = await wx.cloud.callFunction({
  //       name: "msgCheck",
  //       data: {
  //         record: content.resourse.records[i].fileID,
  //       }
  //     })
  //   }
  // }
  // 检测图片是否违规
  // if (content.resourse.images && content.resourse.images.length) {
  //   for (var i = 0; i < content.resourse.images.length; i++) {
  //     var images = []
  //     images[i] = await wx.cloud.callFunction({
  //       name: "msgCheck",
  //       data: {
  //         image: content.resourse.images[i].src,
  //       }
  //     })
  //   }
  // }
  // console.log('测试标题结果', title.result.error)
  // console.log('测试内容结果', html.result.error)
  // 录音和图片暂时不支持云开发调用
  // console.log('测试录音结果', records)
  // console.log('测试图片结果', images)
  if (title.result.error) {
    return ['title', false]
  } else if (html.result.error) {
    return ['html', false]
  } else {
    return ['all', true]
  }
}

export async function getBarcodeInfo(code = '6921804700306') {
  return;
  //签名生不出来 暂缓
  let sign = makeSign();
  console.log("签名", sign)
  let res = await wxp.request({
    url: `https://service-32z6n3ab-1255468759.ap-shanghai.apigateway.myqcloud.com/release/chkBarCode/&code=${code}`,
    header: {
      "Source": sign.source,
      "X-Date": sign.dateTime,
      "Authorization": sign.sign
    }

  })
  console.log("条码信息查询", res.data)

}