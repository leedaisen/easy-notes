/**
 * 分类配置
 * title 分类名称
 * topbar 该分类下新建编辑时 顶部栏的内容共以下五种
 *      date          日期
 *      weather       天气
 *      localtion     位置
 *      tag           标签
 *      code          条形码
 */

//腾讯地图位置信息API key
export const TENCENT_MAP_KEY = ""
//和风天气API key
export const HE_WEATHER_KEY = ""
// 云开发环境ID

/* 线上环境 */
export const CLOUND_ENV = ""
/* 开发环境 */
// export const CLOUND_ENV = "swim-9527"

// 常量储存
export const NOTES_CATE = [{
    title: '会议记录',
    topbar: ["date"]
  },
  {
    title: '课堂笔记',
    topbar: ["date", "tag"]
  },
  {
    title: '私密日记',
    topbar: ["date", "weather", "localtion"]
  },
  {
    title: '产品说明',
    topbar: ["code"]
  }
]

export const MENU_LIST = [{
    icon: 'huiyi',
    name: '会议',
    isActive: false
  },
  {
    icon: 'biaoqianA01_biji-99',
    name: '课堂',
    isActive: false
  },
  {
    icon: 'icon',
    name: '日记',
    isActive: false
  },
  {
    icon: 'zichanshuoming',
    name: '说明',
    isActive: false
  }
]
export const TOOL_BAR = [{
    name: '',
    icon: 'icon icon-ziti text-bold',
    key: ''
  },
  {
    name: '插入图片',
    icon: 'cuIcon-picfill',
    key: ''
  },
  {
    name: '',
    icon: 'cuIcon-voicefill ',
    key: ''
  },
  {
    icon: 'cuIcon-file'
  },
  {
    name: '',
    icon: 'cuIcon-roundaddfill',
    key: ''
  }
]

export const RESOURSES = [{
    name: "音频"
  },
  {
    name: "视频"
  }
]

export const IMAGES_LIST = [
  "https://s1.ax1x.com/2020/09/14/wBGXcT.png", // type === 1 
  "https://s1.ax1x.com/2020/09/14/wBGxuF.png", // type === 2 
  "https://s1.ax1x.com/2020/09/14/wBGjjU.png", // type === 3
  "https://s1.ax1x.com/2020/09/14/wBGO3V.png", // type === 4
  "https://s1.ax1x.com/2020/09/14/wBJD2V.png", // about bg === 4
]

// export const IMAGES_LIST = [
//   "https://6561-easynotes-4gzeqlit253602c0-1256061507.tcb.qcloud.la/static/bg-type-1.jpg", // type === 1 
//   "https://6561-easynotes-4gzeqlit253602c0-1256061507.tcb.qcloud.la/static/bg-type-2.jpg", // type === 2 
//   "https://6561-easynotes-4gzeqlit253602c0-1256061507.tcb.qcloud.la/static/bg-type-3.jpg", // type === 3
//   "https://6561-easynotes-4gzeqlit253602c0-1256061507.tcb.qcloud.la/static/bg-type-4.jpg", // type === 4
//   "https://s1.ax1x.com/2020/09/14/wBJD2V.png", // about bg === 4
// ]


export const ABOUT_TEXT = [{
    title: 'OCR功能介绍',
    content: 'OCR是光学字符识别的缩写，OCR技术简单来说就是将文字信息转换为图像信息，然后再利用文字识别技术将图像信息转化为可以使用的输入技术。',
    list: [
      "1、OCR识别技术不仅具有可以自动判断、拆分、 识别和还原各种通用型印刷体表格，还在表格理解上做出了令人满意的实用结果。",
      "2、OCR能够自动分析文稿的版面布局，自动分栏、并判断出标题、横栏、图像、表格等相应属性，并判定识别顺序，能将识别结果还原成与扫描文稿的版面布局一致的新文本。",
      "3、OCR还可以支持表格自动录入技术，可自动识别特定表格的印刷或打印汉字、字母、数字，可识别手写体汉字、手写体字母、数字及多种手写符号，并按表格格式输出。提高了表格录入效率，可节省大量人力。"
    ]
  },
  {
    title: 'ASR功能介绍',
    content: 'ASR 语音转文本服务能轻松将音频和语音转换为书面文本，以便快速记录内容。',
    list: [
      "1、给音频进行信号处理后，便要按帧（毫秒级）拆分，并对拆分出的小段波形按照人耳特征变成多维向量信息",
      "2、将这些帧信息识别成状态（可以理解为中间过程，一种比音素还要小的过程）",
      "3、再将状态组合形成音素（通常3个状态=1个音素）",
      "4、最后将音素组成字词（dà jiā hǎo）并串连成句 。于是，这就可以实现由语音转换成文字了。"
    ]
  },
  {
    title: 'Bigbang 功能介绍',
    content: 'Bigbang大爆炸 名字借鉴罗太君锤子手机里的同名功能，本质就是中文分词技术 分词技术就是搜索引擎针对用户提交查询的关键词串进行的查询处理后根据用户的关键词串用各种匹配方法进行分词的一种技术。',
    list: [
      "1、OCR识别后返回的文字进行分词处理 在弹出的Bigbang弹窗中用户自行选择 组合并进行微调修改 ",
      "2、语音识别后也可以进行上述操做 提高识别结果的容错率"
    ]
  },
  {
    title: 'TTS 功能介绍',
    content: 'TTS是Text To Speech的缩写，即“从文本到语音”，是人机对话的一部分，让机器能够说话。',
    list: [
      "1、在文章详情页中 点击朗读按钮 可以朗读文章内容",
      "2、TTS是语音合成应用的一种，它将储存于电脑中的文件，如帮助文件或者网页，转换成自然语音输出",
      "3、TTS不仅能帮助有视觉障碍的人阅读计算机上的信息，更能增加文本文档的可读性",

    ]
  }
]