import {
	tz,
	toast,
	vbs,
	toEnTimes,
	sleep,
	getWeather,
	back,
	getUserInfo
} from "../../common/utils"
import {
	IMAGES_LIST,
	NOTES_CATE,
	RESOURSES
} from '../../common/config'
import {
	dbdelete
} from "../../common/requset"
import {
	getNotesDetail
} from "../../common/api/index";
Page({
	data: {
		timer: 24,
		customBackground: '',
		imagesList: IMAGES_LIST,
		type: 4,
		resourses: RESOURSES,
		showResourse: false,
		curResourse: 0,
		resourseNum: 0,
		permissions: {
			edit: false
		},
	},
	async onLoad(options) {
		let {
			title,
			type,
			id
		} = options;
		this.setData({
			title,
			type,
			nowDate: toEnTimes(new Date())
		})
		if (id) {
			this.loadData(id);
		} else {
			toast("记录不存在");
			await sleep();
			back();
		}
	},
	//加载数据
	async loadData(id) {
		let detail = await getNotesDetail(id);
		let resourse = detail.resourse
		this.setData({
			detail,
			title: detail.title,
			content: resourse,
			resourseNum: (resourse.videos ? resourse.videos.length : 0) + (resourse.records ? resourse.records.length : 0) + (resourse.images ? resourse.images.length : 0)
		});
		this.initTopBar();
		this.checkPermission();

	},
	//权限检查
	async checkPermission() {
		let {
			detail
		} = this.data;
		let author = detail.user
		let userInfo = await getUserInfo()
		this.setData({
			['permissions.edit']: author.openid === userInfo.openid
		})
		// console.log("权限检查",author.openid,userInfo.openid)

	},
	//初始化顶部栏
	async initTopBar() {
		let {
			type,
			id
		} = this.data.detail;
		let {
			topbar
		} = NOTES_CATE[type - 1];
		let obj = {};
		topbar.forEach(item => {
			obj[item] = true
		})
		this.setData({
			topbar: obj
		})
		if (obj.code && !id) { //如果该页面需要条形码
			await getBarcodeInfo();
			let {
				confirm
			} = await wx.showModal({
				title: '输入条形码',
				content: '是否立即输入条形码',
				confirmText: '扫描',
				cancelText: '稍后'
			});
			if (confirm) {
				this.handleScanCode();
			}
		}
		if (obj.weather || obj.localtion) { //如果需要天气或地理位置
			let weatherInfo = await getWeather();
			this.setData({
				weatherInfo
			})
		}
	},
	//跳转编辑
	handleEdit() {
		tz(`../editor/editor?id=${this.data.detail._id}&type=${this.data.detail.type}`)
	},
	// 删除
	handleDel(e) {
		wx.showModal({
			cancelText: '取消',
			confirmText: '确定删除',
			content: '删除将无法恢复，是否继续？',
			showCancel: true,
			title: '温馨提醒',
			success: (result) => {
				if (result.confirm) {
					let id = this.data.detail._id
					dbdelete("notes", id).then(res => {
						if (res.result) {
							toast("删除成功，即将返回.");
							sleep(1).then(() => {
								back(1)
							})
						}
					})
				}
			},
			fail: (res) => {},
			complete: (res) => {},
		})
	},
	//切换资源列表
	handleSwitchResourse({
		target: {
			dataset
		}
	}) {
		let {
			index
		} = dataset;
		this.setData({
			curResourse: index
		})
		vbs()
	},
	//显示资源列表
	handleShowResourse() {
		this.setData({
			showResourse: true
		})
	},
	onReady() {},
	onPageScroll(e) {
		if (e.scrollTop >= 100) {
			this.setData({
				customBackground: "bg-white"
			})
		} else {
			this.setData({
				customBackground: "bg-none"
			})
		}
	},
	onShow() {
		var that = this;
		that.data.setInter = setInterval(
			function () {
				if (that.data.timer < 88) {
					that.setData({
						timer: that.data.timer + 12
					})
				} else {
					clearInterval(that.data.setInter)
					that.setData({
						timer: 100
					})
				}
			}, 250);
	},
	onHide() {},
	onUnload() {},
	onPullDownRefresh() {},
	onReachBottom() {},
	// 分享给好友
	onShareAppMessage() {
		let {
			title,
			type
		} = this.data
		let id = this.data.detail._id
		return {
			title: `这是我记录的${title}`,
			path: `pages/detail/detail?id=${id}&type=${type}&title=${title}`
		}
	},
	// 分享到朋友圈
	onShareTimeline() {

	}
})