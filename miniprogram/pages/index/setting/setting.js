import {
  ABOUT_TEXT,
  IMAGES_LIST
} from '../../../common/config'
import {
  getUserInfo,
  updateUserInfo,
  getTimeState
} from '../../../common/utils'
Page({
  data: {
    user: {},
    timeState: ``,
    aboutText: ABOUT_TEXT,
    imagesList: IMAGES_LIST
  },
  onLoad(options) {
    getUserInfo().then(res => {
      this.setData({
        user: res
      })
    })
  },
  onReady() {},
  onShow() {
    this.setData({
      timeState: getTimeState()
    })
  },
  onHide() {},
  onUnload() {},
  onPullDownRefresh() {},
  onReachBottom() {},
  onShareAppMessage() {},
  copyWechat() {
    wx.showModal({
      cancelText: '不要复制',
      confirmText: '立刻马上',
      content: 'φ(>ω<*) 是否要复制作者微信？',
      showCancel: true,
      title: '温馨提醒',
      success: (result) => {
        if (result.confirm) {
          wx.showActionSheet({
            itemList: ['小生的微信', '钢蛋的微信'],
            itemColor: 'itemColor',
            success: (result) => {
              console.log(result.tapIndex)
              if (result.tapIndex === 0) {
                wx.setClipboardData({
                  data: 'Yay_ls',
                })
              } else if (result.tapIndex === 1) {
                wx.setClipboardData({
                  data: 'LIANFANGTI97',
                })
              }
            },
            fail: (res) => {},
            complete: (res) => {},
          })
        }
      },
      fail: (res) => {},
      complete: (res) => {},
    })
  },
  payMoey() {
    wx.showModal({
      cancelText: '最近很穷',
      confirmText: '给点小费',
      content: '(╥╯^╰╥) 软件开发不易，是否要支持一下呢？',
      showCancel: true,
      title: '温馨提醒',
      success: (result) => {
        if (result.confirm) {
          wx.previewImage({
            urls: ['https://s1.ax1x.com/2020/07/10/UMgri8.jpg'],
          })
        }
      },
      fail: (res) => {},
      complete: (res) => {},
    })
  },
  back() {
    wx.navigateBack({
      delta: 1
    })
  },

  userLogin(e) {
    if (e.detail.rawData) {
      updateUserInfo(JSON.parse(e.detail.rawData)).then(res => {
        this.setData({
          user: res
        })
      })
    } else {
      wx.showModal({
        confirmText: '好的~',
        content: '如果你拒绝授权，我觉得你是在不配合我！马上授权！好么？宝贝！',
        showCancel: false,
        title: '温馨提示',
      })
    }
  }
})