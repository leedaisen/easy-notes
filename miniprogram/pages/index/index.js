import {
	tz,
	toast,
	vbs,
	getUserInfo
} from "../../common/utils"
import {
	dbrows
} from "../../common/requset"
import {
	NOTES_CATE,
	MENU_LIST,
	IMAGES_LIST
} from '../../common/config'
Page({
	data: {
		TabCur: 0,
		scrollLeft: 0,
		showSearch: false,
		navList: NOTES_CATE,
		capsule: {},
		user: {},
		list1: [],
		list2: [],
		list3: [],
		list4: [],
		showRefrsher: false,
		imagesList: IMAGES_LIST,
		statusBarHeight: 50,
		menuList: MENU_LIST,
		avatarList: [
			"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2796144188,439704386&fm=26&gp=0.jpg",
			"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2271338977,1611087163&fm=26&gp=0.jpg",
			"https://dss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2156833431,1671740038&fm=26&gp=0.jpg",
			"https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1709644436,2739756540&fm=26&gp=0.jpg",
			"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3302576574,237334394&fm=26&gp=0.jpg",
		]
	},
	async onLoad(options) {
		wx.hideShareMenu() //主页设置禁用分享
		wx.getSystemInfo({
			success: e => {
				let capsule = wx.getMenuButtonBoundingClientRect()
				this.setData({
					capsule: capsule.bottom + capsule.top - e.statusBarHeight
				});
			}
		})
		this.setData({
			list1: await this.getList(1),
			list2: await this.getList(2),
			list3: await this.getList(3),
			list4: await this.getList(4)
		})

	},
	onReady() {},
	onShow() {
		let isSave = wx.getStorageSync('isSave')
		if (isSave) {
			this.setData({
				TabCur: isSave.type - 1
			})
			this.refrsherData()
			wx.removeStorageSync('isSave')
		}
	},
	onHide() {},
	onUnload() {},
	onPullDownRefresh() {},
	onReachBottom() {},
	onShareAppMessage() {},
	// 标题栏切换事件
	tabSelect(e) {
		if (this.data.TabCur == e.currentTarget.dataset.id) {
			return
		} else {
			vbs();
			this.setData({
				TabCur: e.currentTarget.dataset.id,
				scrollLeft: (e.currentTarget.dataset.id - 1) * 60
			})
		}
	},
	// 设置
	toSetting(e) {
		tz("setting/setting")
	},
	// 显示搜索
	showSearch(e) {
		this.setData({
			showSearch: !this.data.showSearch
		})
		vbs();
	},
	// 扫码
	scanErCode() {
		wx.scanCode({
				scanType: "barCode",
				onlyFromCamera: true,
			})
			.then(res => {
				if (!res.result) return false
				let where = {
					barcode: res.result
				}
				dbrows('notes', where, 20, 0, [], [], [], [], true).then(response => {
					if (response.length) {
						let item = response[0]
						tz("/pages/detail/detail?title=" + item.title + "&type=" + item.type + "&id=" + item._id)
					} else {
						toast("没有找到结果~")
					}
				})
				vbs();
			})
			.catch(err => {
				//执行失败，捕获then方法中抛出的错误
				console.log(err)
			})
	},
	// 悬浮按钮组件回调事件
	setBtns(res) {
		let index = res.detail[1]
		if (!index) toast("未开放")
		else tz("/pages/editor/editor?type=" + (Number(index) + 1))
		this.setData({
			menuList: res.detail[0]
		})
	},
	goDetail(e) {
		let {
			item
		} = e.currentTarget.dataset
		tz("/pages/detail/detail?title=" + item.title + "&type=" + item.type + "&id=" + item._id)
	},
	/* 列表获取方法 */
	async getList(type) {
		let user = wx.getStorageSync('USER_INFO')
		if (!user) {
			user = await getUserInfo()
			wx.setStorageSync('USER_INFO', user)
		}
		let where = {
			user: {
				openid: user.openid
			},
			type: type
		}
		let cur = this.data.TabCur + 1;
		let listLength = this.data[`list${cur}`].length
		return dbrows('notes', where, 10, listLength, [], [], [], [], true)
	},

	async loadMore() {
		let cur = this.data.TabCur + 1
		this.setData({
			['list' + cur]: this.data['list' + cur].concat(await this.getList(cur))
		})
	},

	async refrsherData() {
		let cur = this.data.TabCur + 1
		this.setData({
			[`list` + cur]: []
		})
		this.setData({
			showRefrsher: false,
			[`list` + cur]: await this.getList(cur),
		})
	},

	tabSwiper(e) {
		this.setData({
			TabCur: e.detail.current
		})
	}
})